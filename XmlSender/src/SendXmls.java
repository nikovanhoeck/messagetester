import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;
import org.exolab.castor.xml.util.resolvers.CastorXMLStrategy;

import javax.xml.parsers.SAXParser;
import javax.xml.stream.XMLOutputFactory;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class SendXmls {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyManagementException, URISyntaxException {

        String uri = "amqp://eitmyywb:aJi1NXbiDYQcTMC_TJnvmnQz3cBLjxbA@impala.rmq.cloudamqp.com/eitmyywb";
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri); // indien je met een lokaal geinstalleerde RabbitMQ server werkt doe je hier setHost

        //Recommended settings
        factory.setRequestedHeartbeat(30);
        factory.setConnectionTimeout(30000);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        String queue = "TestTrainMessages";     //queue name
        boolean durable = false;    //durable - RabbitMQ will never lose the queue if a crash occurs
        boolean exclusive = false;  //exclusive - if queue only will be used by one connection
        boolean autoDelete = false; //autodelete - queue is deleted when last consumer unsubscribes

        channel.queueDeclare(queue, durable, exclusive, autoDelete, null);

        pollConsole(channel, queue); // endless loop unit exit typed (!)

        channel.close();
        connection.close();

    }

    /**
     * Endless loop unit exit is typed by the user
     */
    private static void pollConsole(Channel channel, String queue) throws IOException {

            Scanner scanner = new Scanner(System.in);
            String msg;
            int choice;
            choice = scanner.nextInt();
            Writer writer = new StringWriter();
            Object xml;
            switch (choice) {
                case 0:
                    System.exit(0);break;
                case 1:
                    try {
                       Marshaller.marshal(new RideMessage("01", "s3", "dl", "10"),writer);
                    } catch (MarshalException e) {
                        e.printStackTrace();
                    } catch (ValidationException e) {
                        e.printStackTrace();
                    };break;
                case 2:
                    try {
                        Marshaller.marshal(new SignalMessage("01", true),writer);
                    } catch (MarshalException e) {
                        e.printStackTrace();
                    } catch (ValidationException e) {
                        e.printStackTrace();
                    };break;
            }
            channel.basicPublish("", queue, null, writer.toString().getBytes());
    }
}
