public class RideMessage extends RailroadMessage {
     private final String sectionId;
     private final String direction;
     private final String blockI;

    public RideMessage(String rideId, String sectionId, String direction, String blockI) {
        super(rideId);
        this.sectionId = sectionId;
        this.direction = direction;
        this.blockI = blockI;
    }

    public String getSectionId() {
        return sectionId;
    }

    public String getDirection() {
        return direction;
    }

    public String getBlockI() {
        return blockI;
    }
}
