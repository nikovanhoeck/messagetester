public class SignalMessage extends Message{

    private final String railwayBarrierId;
    private final boolean status;

    public SignalMessage(String railwayBarrierId, boolean status) {
        this.railwayBarrierId = railwayBarrierId;
        this.status = status;
    }

    public String getRailwayBarrierId() {
        return railwayBarrierId;
    }
}
