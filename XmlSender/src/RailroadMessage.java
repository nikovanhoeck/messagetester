public class RailroadMessage extends Message {

    private final String rideId;

    public RailroadMessage(String rideId) {
        this.rideId = rideId;
    }

    public String getRideId() {
        return rideId;
    }
}
